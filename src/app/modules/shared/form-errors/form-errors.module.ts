import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormErrorsDirective } from './directives/form-errors.directive';
import {FormErrorsService} from "./services/form-errors.service";



@NgModule({
  declarations: [
    FormErrorsDirective
  ],
  imports: [
    CommonModule,
  ],
  exports: [
      FormErrorsDirective
  ],
  providers: [
      FormErrorsService
  ]
})
export class FormErrorsModule { }
