import {Injectable} from '@angular/core';
import {FormControl} from "@angular/forms";
import {FORM_ERROR_MESSAGES, FORM_ERROR_REPLACER} from "../constants/form-error-messages";

@Injectable()
export class FormErrorsService {

    constructor() {
    }

    getErrorMessage(control: FormControl): string {
        for(let i in control.errors) {
            switch (i) {
                case 'maxlength': return FORM_ERROR_MESSAGES.maxlength.replace(FORM_ERROR_REPLACER, control.errors[i].requiredLength);
                case 'required': return FORM_ERROR_MESSAGES.required;
            }
        }

        return FORM_ERROR_MESSAGES.default;
    }
}
