import {Directive, ElementRef, Injector, OnInit, Renderer2} from '@angular/core';
import {FormControl, NgControl} from "@angular/forms";
import {tap} from "rxjs";
import {FormErrorsService} from "../services/form-errors.service";

const ERROR_CLASS_NAME = 'invalid-feedback';
const INVALID_FIELD_CLASS_NAME = 'is-invalid';

@Directive({
  selector: '[appFormErrors]'
})
export class FormErrorsDirective implements OnInit{
  private errorTpl?: HTMLElement;
  constructor(
      private injector: Injector,
      private elementRef: ElementRef,
      private renderer: Renderer2,
      private service: FormErrorsService
  ) { }

  ngOnInit(): void {
    this.control.statusChanges
        .pipe(
            tap(status => {
                status === 'INVALID' ? this.showErrorMessage() : this.hideErrorMessage();
            })
        )
        .subscribe();
  }

  private get control(): FormControl {
    return this.injector.get(NgControl, null)?.control as FormControl;
  }

  private showErrorMessage(): void {
    this.hideErrorMessage();
    this.errorTpl = this.renderer.createElement('div');
    this.errorTpl!.classList.add(ERROR_CLASS_NAME);
    this.elementRef.nativeElement.classList.add(INVALID_FIELD_CLASS_NAME);
    this.errorTpl!.innerHTML = this.service.getErrorMessage(this.control);
    this.renderer.appendChild(this.elementRef.nativeElement.parentNode, this.errorTpl)
  }

  private hideErrorMessage(): void {
    if(this.errorTpl) {
      this.renderer.removeChild(this.errorTpl!.parentNode, this.errorTpl);
      this.elementRef.nativeElement.classList.remove(INVALID_FIELD_CLASS_NAME);
    }
  }
}
