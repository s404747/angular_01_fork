export const FORM_ERROR_REPLACER = '${replacer}';

export const FORM_ERROR_MESSAGES = {
    required: 'the field is required',
    maxlength: 'the field characters shouldn\'t be more then ' + FORM_ERROR_REPLACER,
    default: 'the field isn\t valid'
}

