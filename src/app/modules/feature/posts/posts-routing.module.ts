import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostsListComponent } from './pages/posts-list/posts-list.component';
import { PostsSingleComponent } from './pages/posts-single/posts-single.component';
import { PostsComponent } from './pages/posts/posts.component';
import {PostsCreateComponent} from "./pages/posts-create/posts-create.component";

const routes: Routes = [
  {
    path: '',
    component: PostsComponent,
    children: [
      {
        path: '',
        component: PostsListComponent,
      },
      {
        path: 'create',
        component: PostsCreateComponent,
      },
      {
        path: ':id',
        component: PostsSingleComponent,
      },

    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostsRoutingModule {}
