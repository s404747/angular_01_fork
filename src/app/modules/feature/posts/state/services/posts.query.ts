import { QueryEntity } from '@datorama/akita';
import {PostsState, PostsStore} from "./posts.store";
import {Injectable} from "@angular/core";

@Injectable()
export class PostsQuery extends QueryEntity<PostsState> {
    constructor(protected override store: PostsStore) {
        super(store);
    }
}
