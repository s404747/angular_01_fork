import { PostsApiService } from './posts.api.service';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {of, throwError} from "rxjs";
import {Post} from "../models/post.model";
import {PostsStore} from "./posts.store";
import {TestBed} from "@angular/core/testing";
import {PostsComponent} from "../../pages/posts/posts.component";
import {PostsQuery} from "./posts.query";

const testingPosts: Post[] = [
  new Post({id: 1, title: 'post 1', body: 'body 1', userId: 1}),
  new Post({id: 2, title: 'post 2', body: 'body 2', userId: 2}),
];

describe('Posts.ApiService', () => {
  let service: PostsApiService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostsComponent ],
      providers: [PostsStore, PostsQuery]
    }).compileComponents();

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);

    service = new PostsApiService(httpClientSpy, TestBed.get(PostsStore));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return expected posts', (done: DoneFn) => {
    const data = testingPosts;

    httpClientSpy.get.and.returnValue(of(data));

    service.fetch().subscribe({
      next: posts => {
        expect(posts)
            .withContext('expected posts')
            .toEqual(data);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.get.calls.count())
        .withContext('one call')
        .toBe(1);
  });

  it('should return expected post', (done: DoneFn) => {
    const data = testingPosts[0];

    httpClientSpy.get.and.returnValue(of(data));

    service.get(data.id).subscribe({
      next: posts => {
        expect(posts)
            .withContext(`expected post ${data.title}`)
            .toEqual(data);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.get.calls.count())
        .withContext('one call')
        .toBe(1);
  });

  it('should return an error if post doesn\'t exists', (done: DoneFn) => {
    const data = testingPosts[0].id;
      const errorResponse = new HttpErrorResponse({
          error: '404 Not Found',
          status: 404, statusText: 'Not Found'
      });

    httpClientSpy.get.and.returnValue(throwError(errorResponse));

    service.get(data).subscribe({
      next: () => done.fail('expected an error, not posts'),
      error: error  => {
        expect(error.message).toContain('404 Not Found');
        done();
      }
    });
  });

  it('should update expected post', (done: DoneFn) => {
    const data = testingPosts[0];

    httpClientSpy.patch.and.returnValue(of(data));

    service.update(data.id, data).subscribe({
      next: posts => {
        expect(posts)
            .withContext(`expected updated post ${data.title}`)
            .toEqual(data);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.patch.calls.count())
        .withContext('one call')
        .toBe(1);
  });

  it('should return an error if post doesn\'t exists', (done: DoneFn) => {
    const data = testingPosts[0];
    const errorResponse = new HttpErrorResponse({
      error: '404 Not Found',
      status: 404, statusText: 'Not Found'
    });

    httpClientSpy.patch.and.returnValue(throwError(errorResponse));

    service.update(data.id, data).subscribe({
      next: () => done.fail('expected an error, not posts'),
      error: error  => {
        expect(error.message).toContain('404 Not Found');
        done();
      }
    });
  });

  it('should return an error if post data isn\'t valid', (done: DoneFn) => {
    const data = testingPosts[0];
    const errorResponse = new HttpErrorResponse({
      error: '422 Validation Failed',
      status: 422, statusText: 'Validation Failed'
    });

    httpClientSpy.patch.and.returnValue(throwError(errorResponse));

    service.update(data.id, {title: '', body: ''}).subscribe({
      next: () => done.fail('expected an error, not posts'),
      error: error  => {
        expect(error.message).toContain('422 Validation Failed');
        done();
      }
    });
  });

  it('should create new post', (done: DoneFn) => {
    const data = testingPosts[0];

    httpClientSpy.post.and.returnValue(of(data));

    service.create(data).subscribe({
      next: posts => {
        expect(posts)
            .withContext(`expected created post ${data.title}`)
            .toEqual(data);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.post.calls.count())
        .withContext('one call')
        .toBe(1);
  });

  it('should return an error if post data isn\'t valid', (done: DoneFn) => {
    const errorResponse = new HttpErrorResponse({
      error: '422 Validation Failed',
      status: 422, statusText: 'Validation Failed'
    });

    httpClientSpy.post.and.returnValue(throwError(errorResponse));

    service.create({title: '', body: ''}).subscribe({
      next: () => done.fail('expected an error, not posts'),
      error: error  => {
        expect(error.message).toContain('422 Validation Failed');
        done();
      }
    });
  });

});
