import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import {Post} from "../models/post.model";
import {Injectable} from "@angular/core";

export interface PostsState extends EntityState<Post, number> { }

@Injectable()
@StoreConfig({ name: 'posts' })
export class PostsStore extends EntityStore<PostsState> {
    constructor() {
        super() ;
    }
}
