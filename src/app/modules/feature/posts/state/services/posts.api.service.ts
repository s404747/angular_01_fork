import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map, Observable, tap} from 'rxjs';
import {environment} from "../../../../../../environments/environment";
import {Post} from "../models/post.model";
import {PostData} from "../models/post-data.interface";
import {PostsStore} from "./posts.store";

@Injectable()
export class PostsApiService {
  private url = environment.restUrl;

  constructor(private http: HttpClient, private store: PostsStore) {}

  fetch(): Observable<Post[]> {
    return this.http.get<PostData[]>(this.getUrl(''))
        .pipe(
            map(posts => posts.map(it => new Post(it))),
            tap(posts => this.store.upsertMany(posts))
        );
  }

  get(id: number): Observable<Post> {
    return this.http.get<Post>(this.getUrl(`${id}`))
        .pipe(
            map(postData => new Post(postData)),
            tap(post => this.store.upsert(post.id, post))
        );
  }

  create(post: PostData): Observable<Post> {
    return this.http.post<Post>(this.getUrl(''), post)
        .pipe(
            map(postData => new Post(postData)),
            tap(post => this.store.upsert(post.id, post))
        );
  }

  update(id: number, post: PostData): Observable<Post> {
    return this.http.patch<Post>(this.getUrl(`${id}`), post)
        .pipe(
            map(postData => new Post(postData)),
            tap(post => this.store.upsert(post.id, post))
        );
  }

  private getUrl(url: string): string {
    return `${this.url}/posts/${url}`;
  }
}
