import {PostData} from "./post-data.interface";

export class Post {
  id!: number;
  userId!: number;
  title!: string;
  body!: string;

  constructor(data: PostData) {
    this.id = data.id!;
    this.userId = data.userId!;
    this.title = data.title!;
    this.body = data.body!;
  }
}
