import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PostData} from "../../state/models/post-data.interface";
import {Post} from "../../state/models/post.model";

const BODY_MAX_LENGTH = 450;

@Component({
    selector: 'app-posts-form',
    templateUrl: './posts-form.component.html',
    styleUrls: ['./posts-form.component.scss']
})
export class PostsFormComponent implements OnInit {
    @Output() submitted = new EventEmitter<PostData>();
    @Input() post!: Post;
    @Input() isLoading!: boolean;
    formGroup!: FormGroup;

    constructor(
        private formBuilder: FormBuilder
    ) {
        //
    }

    ngOnInit() {
        this.initForm();
    }

    onSubmit(): void {
        if (this.formGroup.valid) {
            this.submitted.emit(this.formGroup.value);
            this.isLoading = true;
        }
    }

    private initForm(): void {
        this.formGroup = this.formBuilder.group({
            id: this.post.id,
            userId: this.post.userId,
            title: [this.post.title, Validators.required],
            body: [this.post.body, [Validators.required, Validators.maxLength(BODY_MAX_LENGTH)]]
        });
    }

}
