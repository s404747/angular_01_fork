import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {PostsApiService} from "../../state/services/posts.api.service";
import {Observable} from "rxjs";
import {Post} from "../../state/models/post.model";
import {PostsQuery} from "../../state/services/posts.query";

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss'],
})
export class PostsListComponent implements OnInit {
  posts$: Observable<Post[]> = this.query.selectAll();
  isLoading$: Observable<boolean> = this.query.selectLoading();

  constructor(
    private postsApiService: PostsApiService,
    private router: Router,
    private query: PostsQuery
  ) {}

  onPostClick(postId: number): void {
    this.router.navigate([`/posts/${postId}`]);
  }

  ngOnInit(): void {
    this.postsApiService.fetch().subscribe();
  }
}
