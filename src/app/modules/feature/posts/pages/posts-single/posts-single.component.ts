import {Component, OnInit} from '@angular/core';
import {PostsApiService} from "../../state/services/posts.api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {map, Observable, switchMap, tap} from "rxjs";
import {Post} from "../../state/models/post.model";
import {PostData} from "../../state/models/post-data.interface";
import {PostsQuery} from "../../state/services/posts.query";

@Component({
    selector: 'app-posts-single',
    templateUrl: './posts-single.component.html',
    styleUrls: ['./posts-single.component.scss']
})
export class PostsSingleComponent implements OnInit {
    post$!: Observable<Post | undefined>;

    constructor(
        private postsApiService: PostsApiService,
        private activatedRoute: ActivatedRoute,
        private query: PostsQuery,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        this.post$ = this.activatedRoute.params.pipe(
            map(params => params['id']),
            tap(id => this.getPost(id)),
            switchMap(id => this.query.selectEntity(id))
        )
    }

    postSubmitted(data: PostData): void {
        this.postsApiService.update(data.id!, data)
            .pipe(
                tap(() => this.router.navigate(['/posts']))
            )
            .subscribe();
    }

    private getPost(id: number): void {
        this.postsApiService.get(id).subscribe();
    }
}
