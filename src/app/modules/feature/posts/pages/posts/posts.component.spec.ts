import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  RouterTestingModule
} from '@angular/router/testing';
import { PostsComponent } from './posts.component';

describe('PostsComponent', () => {
  let component: PostsComponent;
  let fixture: ComponentFixture<PostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostsComponent ],
      imports: [
        RouterTestingModule.withRoutes([])
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display container', () => {
    expect(fixture.nativeElement.querySelector('.container')).toBeTruthy();
  });

  it('should display post listing link', () => {
    expect(fixture.nativeElement.querySelector('#postListingLink').href).toContain('/posts');
  });

  it('should display post create link', () => {
    expect(fixture.nativeElement.querySelector('#postCreateLink').href).toContain('/posts/create');
  });

  it('should display content div', () => {
    expect(fixture.nativeElement.querySelector('.content')).toBeTruthy();
  });
});
