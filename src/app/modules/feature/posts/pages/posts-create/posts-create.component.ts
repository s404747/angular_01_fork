import { Component, OnInit } from '@angular/core';
import {PostsApiService} from "../../state/services/posts.api.service";
import {Router} from "@angular/router";
import {tap} from "rxjs";
import { Post } from '../../state/models/post.model';

@Component({
  selector: 'app-posts-create',
  templateUrl: './posts-create.component.html',
  styleUrls: ['./posts-create.component.scss']
})
export class PostsCreateComponent implements OnInit {
  post = new Post({}); //could be passed default data
  constructor(
      private postsApiService: PostsApiService,
      private router: Router
  ) { }

  ngOnInit(): void {
  }

  postSubmitted(data: any): void {
      this.postsApiService.create(data)
          .pipe(
              tap(() => this.router.navigate(['/posts']))
          )
          .subscribe();
  }

}
