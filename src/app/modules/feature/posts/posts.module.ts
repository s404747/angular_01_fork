import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {PostsListComponent} from './pages/posts-list/posts-list.component';
import {PostsSingleComponent} from './pages/posts-single/posts-single.component';
import {PostsComponent} from './pages/posts/posts.component';
import {PostsRoutingModule} from './posts-routing.module';
import {PostsCreateComponent} from './pages/posts-create/posts-create.component';
import {PostsFormComponent} from './fragments/posts-form/posts-form.component';
import {PostsApiService} from "./state/services/posts.api.service";
import {ReactiveFormsModule} from "@angular/forms";
import {FormErrorsModule} from "../../shared/form-errors/form-errors.module";
import {LoaderModule} from "../../shared/loader/loader.module";
import {PostsQuery} from "./state/services/posts.query";
import {PostsStore} from "./state/services/posts.store";

@NgModule({
    declarations: [PostsComponent, PostsListComponent, PostsSingleComponent, PostsCreateComponent, PostsFormComponent],
    imports: [
        CommonModule,
        PostsRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormErrorsModule,
        LoaderModule
    ],
    providers: [
        PostsApiService,
        PostsQuery,
        PostsStore
    ],
})
export class PostsModule {
}
