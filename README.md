# Clubforce FE test

## Post Module

- Create an add/update post component containing a reactive form
- Add validators to this form (required and max characters 450 for body field )
- Display errors in the html template if field is invalid, and disable the submit button if invalid
- inject post.api.service.ts to allow user to add/update posts
- Refactor posts.module.ts to use state management (NgRx would be preferable)
- Refactor posts-list.component.ts to make the posts property an Observable
- Style components using a scss framework (Bootstrap would be preferable)
- BONUS: Split work above to 7 different commits
- BONUS: Add unit tests to post.api.service.ts and posts.component.ts
